<?php

$shoppingList = '["CVCD", "SDFD", "DDDF", "SDFD"]';
$mappingList = '{"CVCD": {"version": 1,"edition": "X"},
               "SDFD": {"version": 2,"edition": "Z"},
               "DDDF": {"version": 1}
            }';

function createAggregatedList($mappingList, $shoppingList){

    $mappingList = json_decode($mappingList);
    $shoppingList = json_decode($shoppingList);

    foreach($shoppingList as $list){
        $outputArray[$list] = ( 
            function() use ($list, $mappingList){ 
                isset($mappingList->{$list}->quantity)?($mappingList->{$list}->quantity++):($mappingList->{$list}->quantity = 1);
                return $mappingList->{$list};
            } 
        )();
    }
    
    usort($outputArray, function ($item1, $item2) {
        return $item1->version <=> $item2->version;
    });

    return json_encode($outputArray);
    
}

createAggregatedList($mappingList, $shoppingList);