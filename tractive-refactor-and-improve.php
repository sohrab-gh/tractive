<?php

namespace Tractive\ProductAvailabilityReminder\Model;
use \Tractive\ProductAvailabilityReminder\Api\ProductAvailabilityReminderRepositoryInterface;


class ProductAvailabilityReminderRepository implements ProductAvailabilityReminderRepositoryInterface
{
    private $logger;
    private $productAvailabilityReminderFactory;
    private $productAvailabilityReminderResource;
    private $productRepository;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
\Tractive\ProductAvailabilityReminder\Model\ProductAvailabilityReminderFactory $productAvailabilityReminderFactory,
\Tractive\ProductAvailabilityReminder\Model\ResourceModel\ProductAvailabilityReminder $productAvailabilityReminderResource,
\Tractive\ProductAvailabilityReminder\Api\ProductRepositoryInterface $productRepository
    )
    {
        $this->logger = $logger;
        $this->productAvailabilityReminderFactory = $productAvailabilityReminderFactory;
        $this->productAvailabilityReminderResource = $productAvailabilityReminderResource;
        $this->productRepository = $productRepository;
    }

    /**
        * Get customer's info and notify him/her when the product is available
        *
        * @api
        * @param string $email
        * @param string $language
        * @param string $productSku
        * @return array
    */
    public function registerForProductAvailabilityReminder($email, $language, $productSku){
        $this->logger->info("registerForProductAvailabilityReminderStart");
        try {

            /*
                validations 
            */
            if(!$productSku){
                $this->logger->info("registerForProductAvailabilityProductSkuNotAvailable");
                return [['status' => false, 'message' => 'ProductSku is not available', 'code'=>422]];
            }
            if(!$email){
                $this->logger->info("registerForProductAvailabilityEmailNotAvailable");
                return [['status' => false, 'message' => 'Email is not available', 'code'=>422]];
            }
            if(!$language){
                $this->logger->info("registerForProductAvailabilityLanguageNotAvailable");
                return [['status' => false, 'message' => 'Language is not available', 'code'=>422]];
            }
            if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                $this->logger->info("registerForProductAvailabilityEmailSyntaxIncorrect");
                return [['status' => false, 'message' => 'Language is not available', 'code'=>400]];
            }
            /*
                look for product with input SKU
            */
            try{
                $product = $this->productRepository->get($productSku);
            }
            catch (\Magento\Framework\Exception\NoSuchEntityException $e){
                $this->logger->info("registerForProductAvailabilityReminderProductNotFound");
                return [['status' => false, 'message' => "The {$productSku} is not a valid productSku", 'code'=>204]];
            }
            /*
                check if email has already registered
            */
            $productAvailabilityReminderObject = $this->productAvailabilityReminderFactory->create();
            $productAvailabilityReminderObject = $productAvailabilityReminder->addAttributeToFilter('email', array('eq' => $email));
            if($productAvailabilityReminderObject->getSize() > 0){
                $this->logger->info("registerForProductAvailabilityReminderAlreadyExistsEmail");
                return [['status' => false, 'message' => "The {$email} email already exists! ", 'code'=>409]];
            }
            /*
                creating object
            */
            $this->logger->info("registerForProductAvailabilityReminderCreateObject");
            $productAvailabilityReminder = $this->productAvailabilityReminderFactory->create();
            $productAvailabilityReminder->setEmailAddress($email);
            $productAvailabilityReminder->setProductSku($productSku);
            $productAvailabilityReminder->setLanguage($language);
            $this->logger->info("registerForProductAvailabilityReminderAddedItemsToObject");
            $productAvailabilityReminderCreatedObject = $this->productAvailabilityReminderResource->save($productAvailabilityReminder);
            $this->logger->info("registerForProductAvailabilityReminderSaveObjectFinishedSaving");
            return [['status' => true, 'message'=>'Done' ,'item'=>$productAvailabilityReminderCreatedObject , 'code'=>201]];

        } 
        catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            return [['status' => false, 'message' => $e->getMessage(), 'code'=>500]];
        }
    }
}